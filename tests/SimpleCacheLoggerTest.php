<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-simple-cache-logger library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\SimpleCache\SimpleCacheLogger;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheInterface;

/**
 * SimpleCacheLoggerTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\SimpleCache\SimpleCacheLogger
 *
 * @internal
 *
 * @small
 */
class SimpleCacheLoggerTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var SimpleCacheLogger
	 */
	protected SimpleCacheLogger $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new SimpleCacheLogger($this->getMockForAbstractClass(CacheInterface::class), $this->getMockForAbstractClass(LoggerInterface::class));
	}
	
}
