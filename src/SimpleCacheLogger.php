<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-simple-cache-logger library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\SimpleCache;

use DateInterval;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheInterface;
use Stringable;

/**
 * SimpleCacheLogger class file.
 * 
 * This class is an implementation of a cache which does logging each cache
 * call on the underlying cache.
 * 
 * @author Anastaszor
 */
class SimpleCacheLogger implements CacheInterface, Stringable
{
	
	/**
	 * The inner cache.
	 * 
	 * @var CacheInterface
	 */
	protected CacheInterface $_cache;
	
	/**
	 * The inner logger.
	 * 
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * Builds a new SimpleCacheLogger with the given cache and logger.
	 * 
	 * @param CacheInterface $cache
	 * @param LoggerInterface $logger
	 */
	public function __construct(CacheInterface $cache, LoggerInterface $logger)
	{
		$this->_cache = $cache;
		$this->_logger = $logger;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::get()
	 * @param string $key
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $default
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 * @psalm-suppress MixedInferredReturnType,MoreSpecificImplementedParamType
	 */
	public function get(string $key, mixed $default = null) : mixed
	{
		$ret = $this->_cache->get($key, $default);
		
		$this->_logger->debug('{method} {result} on key "{key}"', [
			'method' => 'Cache GET',
			'result' => ($ret === $default ? 'MISS' : 'HIT'),
			'key' => $key,
		]);
		
		/** @psalm-suppress MixedReturnStatement */
		return $ret;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::set()
	 * @param string $key
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @param null|int|DateInterval $ttl
	 * @return boolean
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function set(string $key, mixed $value, null|int|DateInterval $ttl = null) : bool
	{
		$ret = $this->_cache->set($key, $value, $ttl);
		
		$this->_logger->debug('{method} {result} on key "{key}"', [
			'method' => 'Cache SET',
			'result' => ($ret ? 'FAIL' : 'SUCCESS'),
			'key' => $key,
		]);
		
		return $ret;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::delete()
	 */
	public function delete(string $key) : bool
	{
		$ret = $this->_cache->delete($key);
		
		$this->_logger->debug('{method} {result} on key "{key}"', [
			'method' => 'Cache DELETE',
			'result' => ($ret ? 'FAIL' : 'SUCCESS'),
			'key' => $key,
		]);
		
		return $ret;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::clear()
	 */
	public function clear() : bool
	{
		$ret = $this->_cache->clear();
		
		$this->_logger->debug('{method} {result}', [
			'method' => 'Cache CLEAR',
			'result' => ($ret ? 'FAIL' : 'SUCCESS'),
		]);
		
		return $ret;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::getMultiple()
	 * @param iterable<string> $keys
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $default
	 * @return iterable<null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 * @psalm-suppress MoreSpecificImplementedParamType,MixedReturnTypeCoercion,LessSpecificImplementedReturnType
	 */
	public function getMultiple(iterable $keys, mixed $default = null) : iterable
	{
		$this->_logger->debug(
			'{method} on keys "{keys}"',
			['method' => 'Cache GET Multiple', 'keys' => $keys],
		);
		
		/** @psalm-suppress MixedReturnTypeCoercion */
		return $this->_cache->getMultiple($keys, $default);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::setMultiple()
	 * @param iterable<string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $values
	 * @param null|int|DateInterval $ttl
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function setMultiple(iterable $values, null|int|DateInterval $ttl = null) : bool
	{
		$keys = [];
		
		foreach($values as $key => $value)
		{
			$keys[] = (string) $key;
			unset($value);
		}
		
		$this->_logger->debug(
			'{method} with ttl {ttl} on keys "{keys}"',
			['method' => 'Cache SET Multiple', 'ttl' => $ttl, 'keys' => $keys],
		);
		
		return $this->_cache->setMultiple($values, $ttl);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::deleteMultiple()
	 * @param iterable<string> $keys
	 */
	public function deleteMultiple(iterable $keys) : bool
	{
		$this->_logger->debug(
			'{method} on keys "{keys}"',
			['method' => 'Cache DELETE Multiple', 'keys' => $keys],
		);
		
		return $this->_cache->deleteMultiple($keys);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::has()
	 */
	public function has(string $key) : bool
	{
		$this->_logger->debug(
			'{method} on key "{key}"',
			['method' => 'Cache HAS', 'key' => $key],
		);
		
		return $this->_cache->has($key);
	}
	
}
